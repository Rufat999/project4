package com.example.task4.controller;

import com.example.task4.dto.CarDto;
import com.example.task4.dto.CreateCarDto;
import com.example.task4.dto.UpdateCarDto;
import com.example.task4.service.CarService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CarController {
    private final CarService carService;

    public CarController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping("/all")
    public List<CarDto> getAllCars(){
        return carService.getAllCars();
    }

    @GetMapping("/{id}")
    public CarDto getCarById(@PathVariable Integer id){
        return carService.getCarById(id);
    }

    @PostMapping("/add")
    public void createCar(@RequestBody CreateCarDto carDto){
        carService.createCar(carDto);
    }

    @PutMapping("/update/{id}")
    public void deleteById(@PathVariable Integer id, @RequestBody UpdateCarDto updateCarDto){
        carService.update(id, updateCarDto);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteById(@PathVariable Integer id){
        carService.deleteById(id);
    }

    @DeleteMapping("/delete/all")
    public void deleteAllCars(){
        carService.deleteAllCars();
    }
}