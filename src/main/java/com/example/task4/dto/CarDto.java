package com.example.task4.dto;

import lombok.Data;

@Data
public class CarDto {
    private Integer id;
    private String typeOfEngine;
    private String model;
    private int year;
    private String typeOfFuel;
    private String color;
    private String price;
}
