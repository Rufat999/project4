package com.example.task4.service;

import com.example.task4.dto.CarDto;
import com.example.task4.dto.CreateCarDto;
import com.example.task4.dto.UpdateCarDto;

import java.util.List;


public interface CarService {
    List<CarDto> getAllCars();

    CarDto getCarById(Integer id);

    void createCar(CreateCarDto carDto);

    void update(Integer id, UpdateCarDto updateCarDto);

    void deleteById(Integer id);

    void deleteAllCars();
}
