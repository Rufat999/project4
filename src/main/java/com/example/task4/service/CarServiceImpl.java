package com.example.task4.service;

import com.example.task4.dto.CarDto;
import com.example.task4.dto.CreateCarDto;
import com.example.task4.dto.UpdateCarDto;
import com.example.task4.entity.Car;
import com.example.task4.repository.CarRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarServiceImpl implements CarService{

    private final CarRepository carRepository;
    private final ModelMapper modelMapper;

    public CarServiceImpl(CarRepository carRepository, ModelMapper modelMapper) {
        this.carRepository = carRepository;
        this.modelMapper = modelMapper;
    }

    @Override
    public List<CarDto> getAllCars() {
        List<Car> cars = carRepository.findAll();
        List<CarDto> getCarDtoList = cars.stream().map(
                (car) -> modelMapper
                        .map(car, CarDto.class))
                .collect(Collectors.toList());
        return getCarDtoList;
    }

    @Override
    public CarDto getCarById(Integer id) {
        Car car = carRepository.findById(id).get();
        CarDto carDto = modelMapper.map(car, CarDto.class);
        return carDto;
    }

    @Override
    public void createCar(CreateCarDto carDto) {
         Car car = modelMapper.map(carDto, Car.class);
         carRepository.save(car);
    }

    @Override
    public void update(Integer id, UpdateCarDto updateCarDto) {
        Car car = carRepository.findById(id).get();
        car.setTypeOfEngine(updateCarDto.getTypeOfEngine());
        car.setModel(updateCarDto.getModel());
        car.setYear(updateCarDto.getYear());
        car.setTypeOfFuel(updateCarDto.getTypeOfFuel());
        car.setColor(updateCarDto.getColor());
        car.setPrice(updateCarDto.getPrice());

        carRepository.save(car);
    }

    @Override
    public void deleteById(Integer id) {
        carRepository.deleteById(id);
    }

    @Override
    public void deleteAllCars() {
        carRepository.deleteAll();
    }
}
